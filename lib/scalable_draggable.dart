import 'package:flutter/material.dart';


class ScalableDraggable extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Draggable and Resizable Widget'),
        ),
        body: MyResizableWidget(),
      ),
    );
  }
}

class MyResizableWidget extends StatefulWidget {
  @override
  _MyResizableWidgetState createState() => _MyResizableWidgetState();
}

class _MyResizableWidgetState extends State<MyResizableWidget> {
  double _scale = 1.0;
  Offset _dragPosition = Offset(0, 0);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned(
          left: _dragPosition.dx,
          top: _dragPosition.dy,
          child: GestureDetector(
            onScaleUpdate: (details) {
              setState(() {
                _scale = details.scale;
              });
            },
            onPanUpdate: (details) {
              setState(() {
                _dragPosition += details.delta;
              });
            },
            child: Transform.scale(
              scale: _scale,
              child: Container(
                color: Colors.red,
                width: 100,
                height: 100,
                child: Center(
                  child: Text('Drag and Resize me'),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
