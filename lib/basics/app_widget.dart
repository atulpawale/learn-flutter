import 'package:flutter/material.dart';
import 'package:flutter_app/basics/gradient_container.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Scaffold(
        body: GradientContainer([Colors.deepOrange, Colors.purpleAccent])
      ),
    );
  }

}

