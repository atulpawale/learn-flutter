import 'package:flutter/material.dart';

class ToggleButton extends StatelessWidget {
  ToggleButton({
    super.key,
    required this.isTrue,
    this.isBold = false,
    this.isItalic = false,
    this.isUnderline = false,
    required this.text,
  });

  bool isTrue;
  bool isBold;
  bool isItalic;
  bool isUnderline;
  String text;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 4),
        color: isTrue
            ? const Color.fromARGB(255, 135, 174, 243)
            : const Color.fromARGB(255, 212, 211, 211),
        child: Text(
          text,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: isTrue ? Colors.white : Colors.black,
            fontWeight: isBold ? FontWeight.bold : FontWeight.normal,
            fontStyle: isItalic ? FontStyle.italic : FontStyle.normal,
            decoration:
                isUnderline ? TextDecoration.underline : TextDecoration.none,
            decorationColor: isTrue ? Colors.white : Colors.black,
          ),
        ),
      ),
    );
  }
}
