import 'package:flutter/material.dart';
import 'package:flutter_app/test_widgets/toggle_button.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Test Widget',
      home: Scaffold(
          body: Row(
        children: [
          ToggleButton(
            isTrue: true,
            isBold: true,
            text: 'B',
          ),
          const SizedBox(
            width: 1,
          ),
          ToggleButton(
            isTrue: true,
            isItalic: true,
            text: 'I',
          ),
          const SizedBox(
            width: 1,
          ),
          ToggleButton(
            isTrue: true,
            isUnderline: true,
            text: 'U',
          ),
        ],
      )),
    );
  }
}
