import 'package:flutter/material.dart';
import 'package:flutter_app/expense/widgets/chart/chart.dart';
import 'package:flutter_app/expense/widgets/expenses_list/expenses_list.dart';
import 'package:flutter_app/expense/models/expense.dart';
import 'package:flutter_app/expense/widgets/new_expense.dart';

class Expenses extends StatefulWidget {
  const Expenses({super.key});

  @override
  State<Expenses> createState() => _ExpensesState();
}

class _ExpensesState extends State<Expenses> {
  final List<Expense> expenses = [
    Expense(
        title: "Flutter Course",
        amount: 19.99,
        date: DateTime.now(),
        category: Category.work),
    Expense(
        title: "Cinema",
        amount: 15.99,
        date: DateTime.now(),
        category: Category.leisure),
  ];

  void addExpense(Expense expense) {
    setState(() {
      expenses.add(expense);
    });
  }

  void removeExpense(Expense expense) {
    final index = expenses.indexOf(expense);
    setState(() {
      expenses.remove(expense);
    });
    ScaffoldMessenger.of(context).clearSnackBars();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        duration: const Duration(seconds: 3),
        content: const Text('Expensed Deleted'),
        action: SnackBarAction(
          label: 'Undo',
          onPressed: () {
            setState(() {
              expenses.insert(index, expense);
            });
          },
        ),
      ),
    );
  }

  void _openAddForm() {
    showModalBottomSheet(
      useSafeArea: true,
      isScrollControlled: true,
      context: context,
      builder: (ctx) => NewExpense(addExpense: addExpense),
    );
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;

    Widget mainContent = const Center(
      child: Text("No Expenses Added"),
    );
    if (expenses.isNotEmpty) {
      mainContent = Expanded(
        child: ExpensesList(
          expenses: expenses,
          removeExpense: removeExpense,
        ),
      );
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text("Flutter Expense Tracker"),
        actions: [
          IconButton(onPressed: _openAddForm, icon: const Icon(Icons.add))
        ],
      ),
      body: width < 800
          ? Column(
              children: [
                Chart(expenses: expenses),
                mainContent,
              ],
            )
          : Row(children: [
              Expanded(
                child: Chart(expenses: expenses),
              ),
              mainContent,
            ]),
    );
  }
}
