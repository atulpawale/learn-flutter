import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/chat_app/widgets/message_bubble.dart';

class ChatMessages extends StatelessWidget {
  const ChatMessages({super.key});

  @override
  Widget build(BuildContext context) {
    final authUser = FirebaseAuth.instance.currentUser!;

    return StreamBuilder(
      stream: FirebaseFirestore.instance
          .collection('chat')
          .orderBy(
            'createdAt',
            descending: true,
          )
          .snapshots(),
      builder: (ctx, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }

        if (!snapshot.hasData || snapshot.data!.docs.isEmpty) {
          return const Center(
            child: Text('No Messages found.'),
          );
        }

        if (snapshot.hasError) {
          return const Center(
            child: Text('Something went wrong.'),
          );
        }

        final loadedMsgs = snapshot.data!.docs;

        return ListView.builder(
          padding: const EdgeInsets.only(bottom: 40, left: 13, right: 13),
          reverse: true,
          itemCount: loadedMsgs.length,
          itemBuilder: (ctx, index) {
            final msg = loadedMsgs[index].data();
            final nextMsg = index + 1 < loadedMsgs.length
                ? loadedMsgs[index + 1].data()
                : null;

            final userId = msg['userId'];
            final nextUserId = nextMsg != null ? nextMsg['userId'] : null;
            final sameUser = nextUserId == userId;

            if (sameUser) {
              return MessageBubble.next(
                  message: msg['text'], isMe: authUser.uid == userId);
            } else {
              return MessageBubble.first(
                message: msg['text'],
                isMe: authUser.uid == userId,
                userImage: msg['userImage'],
                username: msg['userName'],
              );
            }
          },
        );
      },
    );
  }
}
