import 'package:flutter/material.dart';
import 'package:flutter_app/advanced/data/questions.dart';
import 'package:flutter_app/advanced/questions_sceen.dart';
import 'package:flutter_app/advanced/result_screen.dart';

import 'start_screen.dart';

class QuizApp extends StatefulWidget {
  const QuizApp({super.key});

  @override
  State<QuizApp> createState() => _QuizAppState();
}

class _QuizAppState extends State<QuizApp> {
  String activeScreen = "start_screen";
  List<String> selectedAnswers = [];

  void switchScreen() {
    setState(() {
      activeScreen = "que_screen";
    });
  }

  void chooseAnswer(String answer){
    selectedAnswers.add(answer);

    if(selectedAnswers.length == questions.length){
      setState(() {
        activeScreen = "result_screen";
    });
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget screen = StartScreen(switchScreen);

    if(activeScreen == "que_screen")    {
      screen = QuestionScreen(onSelectAnswer: chooseAnswer,);
    }

     if(activeScreen == "result_screen")    {
      screen = ResultScreen(chosenAnswers: selectedAnswers,);
    }   

    return MaterialApp(
      home: Scaffold(
        body: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
                colors: [Colors.deepPurple, Colors.purpleAccent]),
          ),
          child: screen,
        ),
      ),
    );
  }
}
