import 'package:flutter/material.dart';

class StartScreen extends StatelessWidget {
  final void Function()  switchScreen;
  
  const StartScreen(this.switchScreen, {
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min, 
        children: [
          Opacity(
            opacity: 0.7,
            child: Image.asset(
              'assets/dice1.png',
              width: 200,
            ),
          ),
          const SizedBox(
            height: 80,
          ),
          const Text(
            'Learn the Flutter in Fun Way!!',
            style: TextStyle(
              color: Colors.white,
              fontSize: 24,
            ),
          ),
          const SizedBox(height: 30,),
          OutlinedButton.icon(
            onPressed: switchScreen, 
            icon: const Icon(Icons.arrow_right_alt, color: Colors.white,), 
            label: const Text(
              'Start',
              style: TextStyle(
              color: Colors.white,
              fontSize: 24,
            ),
            )
          ),
      ]),
    );
  }
}
