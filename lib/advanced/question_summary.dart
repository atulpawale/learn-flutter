import 'package:flutter/material.dart';

class QuestionSummary extends StatelessWidget {
  const QuestionSummary(this.summaryData, {super.key});

  final List<Map<String, Object>> summaryData;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 300,
      child: SingleChildScrollView(
        child: Column(
          children: summaryData.map((data){
            return Row(children: [
              Text(((data['que_index'] as int) + 1).toString(),
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 15,
                color: Colors.white
              ),),
              Expanded(
                child: Column(
                  children: [
                    Text(data['que'].toString()),
                    const SizedBox(height: 5,),
                    Text(data['user_answer'].toString()),
                    Text(data['correct_answer'].toString()),
                    const SizedBox(height: 10,),
                  ],
                ),
                
              ),
            ],);
          }).toList(),
        ),
      ),
    );
  }
}