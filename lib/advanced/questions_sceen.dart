import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'answer_button.dart';
import 'package:flutter_app/advanced/data/questions.dart';

class QuestionScreen extends StatefulWidget {
  const QuestionScreen({super.key, required this.onSelectAnswer});

  final void Function(String answer) onSelectAnswer;

  @override
  State<QuestionScreen> createState() => __QuestionScreenState();
}

class __QuestionScreenState extends State<QuestionScreen> {
  int queIndex = 0;

  void answerQue(String answer) {
    widget.onSelectAnswer(answer);
    setState(() {
      queIndex = queIndex + 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    var currentQue = questions[queIndex];
    return SizedBox(
      width: double.infinity,
      child: Container(
        margin: const EdgeInsets.all(40),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              currentQue.text,
              style: GoogleFonts.lato(color: Colors.white, fontSize: 24),
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 30,
            ),
            ...currentQue.getShuffledAnswers().map((ans) {
              return AnswerButton(
                  text: ans,
                  onTap: () {
                    answerQue(ans);
                  });
            }),
          ],
        ),
      ),
    );
  }
}
